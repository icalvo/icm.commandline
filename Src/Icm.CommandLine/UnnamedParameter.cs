namespace Icm.CommandLine
{
	public class UnnamedParameter
	{
	    public UnnamedParameter(string name) : this(name, null)
	    {
	    }

	    public UnnamedParameter(string name, string description)
		{
			this.Name = name;
			this.Description = description;
		}

        public string Name { get; private set; }

	    public string Description { get; private set; }
	}

}
