namespace Icm.CommandLine
{
    public class ListSpec : UnnamedParameter
    {
        public ListSpec(string name) : base(name)
        {
        }

        public ListSpec(string name, string description) : base(name, description)
        {
        }
    }
}