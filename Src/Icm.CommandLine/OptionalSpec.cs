namespace Icm.CommandLine
{
    public class OptionalSpec : UnnamedParameter
    {
        public OptionalSpec(string name) : base(name)
        {
        }

        public OptionalSpec(string name, string description) : base(name, description)
        {
        }
    }
}