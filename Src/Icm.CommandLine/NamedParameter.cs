using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Icm.CommandLine
{
    using res = Resources.CommandLine;

	/// <summary>
	///  Command line option.
	/// </summary>
	/// <remarks></remarks>
	public class NamedParameter
	{
        public NamedParameter(string shortName, string longName, string description, bool required, UnnamedParametersConfig subargs)
		{
            if (subargs == null)
            {
                throw new ArgumentNullException("subargs");
            }

            if (shortName.Length > longName.Length)
	        {
	            throw new ArgumentException("Short name is longer than long name");
	        }

			this.ShortName = shortName;
            this.LongName = longName;
            this.Description = description;
			this.IsRequired = required;
            this.SubParameters = subargs;
		}

		/// <summary>
		///  If true, the presence of this option in the command line makes the processor to
		/// ignore the main arguments because they are unneeded.
		/// </summary>
		/// <value></value>
		/// <returns></returns>
		/// <remarks></remarks>
		public bool MakesMainArgumentsIrrelevant { get; set; }

	    public bool IsRequired { get; private set; }

	    /// <summary>
	    ///  Short option (for example -h)
	    /// </summary>
	    /// <value></value>
	    /// <returns></returns>
	    /// <remarks></remarks>
	    public string ShortName { get; private set; }

	    /// <summary>
	    ///  Long option (for example --help)
	    /// </summary>
	    /// <value></value>
	    /// <returns></returns>
	    /// <remarks></remarks>
	    public string LongName { get; private set; }

	    /// <summary>
		///  Description of this option.
		/// </summary>
		/// <value></value>
		/// <returns></returns>
		/// <remarks></remarks>
		public string Description { get; set; }

        public UnnamedParametersConfig SubParameters { get; private set; }
	}
}
