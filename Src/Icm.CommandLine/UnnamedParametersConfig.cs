using System;
using System.Collections.Generic;
using System.Linq;

namespace Icm.CommandLine
{
    public class UnnamedParametersConfig
	{

		#region "Attributes"

	    private readonly List<UnnamedParameter> argumentNameList = new List<UnnamedParameter>();

	    #endregion

		private void StoreArgList(IEnumerable<UnnamedParameter> argNameList)
		{
			if (argNameList != null) {
				foreach (var argLoopVariable in argNameList) {
					var arg = argLoopVariable;
					if (this.ParametersDictionary.ContainsKey(arg.Name)) {
						if (!ReferenceEquals(this.ParametersDictionary[arg.Name], arg)) {
							throw new ArgumentException(String.Format("You have passed two Unnamed Parameters with the same name ({0}). If they are of the same kind, use the same UnnamedParameter object for both. Otherwise, use different names", arg.Name));
						}
					} else {
						this.ParametersDictionary.Add(arg.Name, arg);
					}
				}
				this.argumentNameList.AddRange(argNameList);
			}

		}

		internal UnnamedParametersConfig()
		{
		    this.ParametersDictionary = new Dictionary<string, UnnamedParameter>();
		    this.Minimum = 0;
			this.Maximum = 0;
		}

		/// <summary>
		/// Initializes a new instance of the UnnamedParametersConfig class.
		/// </summary>
		/// <param name="minimum">Minimum number of arguments</param>
		/// <param name="unboundParameter">Name for the optional arguments from minimum+1 beyond</param>
		/// <param name="argNameList">Names for the required arguments between 0 and minimum</param>
		internal UnnamedParametersConfig(int minimum, UnnamedParameter unboundParameter, IEnumerable<UnnamedParameter> argNameList)
		{
		    this.ParametersDictionary = new Dictionary<string, UnnamedParameter>();
		    if (minimum < 0)
            {
                throw new ArgumentOutOfRangeException("minimum");
            }

            if (argNameList.Count() != minimum)
            {
				throw new ArgumentException(Resources.CommandLine.S_ERR_REQUIRED_MAIN_ARGUMENTS_WITHOUT_NAME);
			}

			this.Minimum = minimum;
			this.Maximum = null;
			this.UnboundParameter = unboundParameter;
			this.StoreArgList(argNameList);
		}

        internal UnnamedParametersConfig(int minimum, int maximum, IEnumerable<UnnamedParameter> argNameList)
		{
		    if (argNameList == null)
		    {
		        throw new ArgumentNullException("argNameList");
		    }

		    this.ParametersDictionary = new Dictionary<string, UnnamedParameter>();
		    CheckMinimumMaximum(minimum, maximum);

            if (argNameList.Count() != maximum)
            {
				throw new ArgumentException(Resources.CommandLine.S_ERR_REQUIRED_MAIN_ARGUMENTS_WITHOUT_NAME);
			}

			this.Minimum = minimum;
			this.Maximum = maximum;
			this.StoreArgList(argNameList);
		}

	    public static UnnamedParametersConfig Optional(string name)
	    {
	        return Optional(new UnnamedParameter(name, ""));
	    }

	    public static UnnamedParametersConfig AtMost(string name)
	    {
	        return AtMost(new UnnamedParameter(name, ""));
	    }

	    public static UnnamedParametersConfig Exactly(string name)
	    {
	        return Exactly(new UnnamedParameter(name, ""));
	    }

	    public static UnnamedParametersConfig Optional(string name, string description)
	    {
	        return Optional(new UnnamedParameter(name, ""));
	    }

	    public static UnnamedParametersConfig AtMost(string name, string description)
	    {
	        return AtMost(new UnnamedParameter(name, ""));
	    }

	    public static UnnamedParametersConfig Exactly(string name, string description)
	    {
	        return Exactly(new UnnamedParameter(name, ""));
	    }


	    /// <summary>
        ///     Establishes that unnamed parameters are optional. This is the default value.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig Optional()
        {
            return Optional(new UnnamedParameter("args", ""));
        }

        /// <summary>
        ///     Establishes that unnamed parameters are optional. This is the default value.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig Optional(UnnamedParameter unboundArgumentsName)
        {
            return new UnnamedParametersConfig(0, unboundArgumentsName, new UnnamedParameter[0]);
        }

        /// <summary>
        ///     Establishes that the number of unnamed parameters must be between min and max.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="argList">Names and descriptions of all arguments be</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig Between(int min, params UnnamedParameter[] argList)
        {
            return new UnnamedParametersConfig(min, argList.Count(), argList);
        }

        /// <summary>
        /// Establishes that at least min unnamed parameters will be required.
        /// </summary>
        /// <param name="unboundArgument">The unbound argument.</param>
        /// <param name="argList">The argument list.</param>
        /// <returns></returns>
        public static UnnamedParametersConfig AtLeast(
            UnnamedParameter unboundArgument,
            params UnnamedParameter[] argList)
        {
            return new UnnamedParametersConfig(argList.Count(), unboundArgument, argList);
        }

        /// <summary>
        ///     Establishes that at most max unnamed parameters will be required.
        /// </summary>
        /// <param name="argList"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig AtMost(params UnnamedParameter[] argList)
        {
            return new UnnamedParametersConfig(0, argList.Count(), argList);
        }

        /// <summary>
        ///     Establishes that exactly num unnamed parameters will be required.
        /// </summary>
        /// <param name="argList"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig Exactly(params UnnamedParameter[] argList)
        {
            return new UnnamedParametersConfig(argList.Count(), argList.Count(), argList);
        }

        /// <summary>
        ///     Establishes that at least one main parameter is required.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig Required(UnnamedParameter requiredArg, UnnamedParameter unboundArgument)
        {
            return new UnnamedParametersConfig(1, unboundArgument, new[] { requiredArg });
        }

        /// <summary>
        ///     Establishes that at least one main parameter is required.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig Required(UnnamedParameter arg)
        {
            return new UnnamedParametersConfig(1, arg, new[] { arg });
        }

        /// <summary>
        ///     Establishes that no unnamed parameters will be admitted.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static UnnamedParametersConfig None()
        {
            return new UnnamedParametersConfig();
        }


	    private static void CheckMinimumMaximum(int minimum, int? maximum)
	    {
	        if (minimum < 0)
	        {
	            throw new ArgumentOutOfRangeException("minimum");
	        }

	        if (maximum.HasValue)
	        {
                if (maximum.Value < 0)
                {
                    throw new ArgumentOutOfRangeException("maximum");
                }

                if (minimum > maximum.Value)
                {
                    throw new Exception("minimum greater than maximum");
                }
	        }
	    }

	    public int Minimum { get; private set; }

        public int? Maximum { get; private set; }

        public UnnamedParameter UnboundParameter { get; private set; }

        public Dictionary<string, UnnamedParameter> ParametersDictionary { get; private set; }

	    public UnnamedParameter GetArgumentName(int i) {
			return this.argumentNameList[i];
		}

	    public void Iterate(Action<UnnamedParameter> requiredAction, Action<UnnamedParameter> optionalAction, Action<UnnamedParameter> listAction)
	    {
            var subargs = new List<string>();

            for (var i = 0; i < this.Minimum; i++)
            {
                requiredAction(this.GetArgumentName(i));
            }

            if (this.Maximum.HasValue)
            {
                for (var i = this.Minimum; i < this.Maximum.Value; i++)
                {
                    optionalAction(this.GetArgumentName(i));
                }
            }
            else
            {
                listAction(this.UnboundParameter);
            }
	    }

	    public bool Exceeds(int count)
	    {
            return !this.Maximum.HasValue || count <= this.Maximum;
	    }
	}

}
