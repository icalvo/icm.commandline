using System;
using System.Linq;

namespace Icm.CommandLine
{
    public static class ParamsBuilder
    {
        public static UnnamedParametersConfig Build(params UnnamedParameter[] paramSpecs)
        {
            var required = paramSpecs
                .TakeWhile(p => p.GetType() == typeof (RequiredSpec));
            var optional = paramSpecs
                .SkipWhile(p => p.GetType() == typeof(RequiredSpec))
                .TakeWhile(p => p.GetType() == typeof(OptionalSpec));
            var list = paramSpecs
                .SkipWhile(p => p.GetType() == typeof(RequiredSpec))
                .SkipWhile(p => p.GetType() == typeof(OptionalSpec))
                .TakeWhile(p => p.GetType() == typeof(ListSpec));

            if (required.Count() + optional.Count() + list.Count() != paramSpecs.Count())
            {
                throw new Exception();
            }

            if (optional.Any() && list.Any())
            {
                throw new Exception();
            }

            if (list.Count() > 1)
            {
                throw new Exception();
            }

            if (list.Any())
            {
                return new UnnamedParametersConfig(required.Count(), list.Single(), required.ToArray());
            }

            return new UnnamedParametersConfig(required.Count(), required.Count() + optional.Count(), required.Union(optional).ToArray());
        }
    }
}