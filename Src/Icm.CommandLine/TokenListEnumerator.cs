using System.Collections.Generic;

namespace Icm.CommandLine
{
    internal class TokenListEnumerator
    {
        private readonly IReadOnlyList<string> list;
        private int index;
        public TokenListEnumerator(IReadOnlyList<string> list)
        {
            this.list = list;

            if (list.Count > 0)
            {
                this.Token = this.list[this.index];
                this.CurrentWithoutDashes = this.Token.TrimStart('-');
                this.IsSeparator = string.IsNullOrEmpty(this.CurrentWithoutDashes);
                this.TokenIsParameter = !this.IsSeparator && this.Token != this.CurrentWithoutDashes;
            }
        }

        public bool MoveNext()
        {
            this.index++;
            if (!this.Finished())
            {
                this.Token = this.list[this.index];
                this.CurrentWithoutDashes = this.Token.TrimStart('-');
                this.IsSeparator = string.IsNullOrEmpty(this.CurrentWithoutDashes);
                this.TokenIsParameter = !this.IsSeparator && this.Token != this.CurrentWithoutDashes;

                return true;
            }

            this.FinishCursor();
            return false;
        }

        private void FinishCursor()
        {
            this.Token = null;
            this.IsSeparator = false;
            this.TokenIsParameter = false;
        }

        public string Token { get; private set; }

        public bool IsSeparator { get; private set; }

        public bool TokenIsParameter { get; private set; }

        internal bool Finished()
        {
            return this.index >= this.list.Count;
        }

        public int Count 
        {
            get
            {
                return this.list.Count;
            }
        }

        public string CurrentWithoutDashes { get; set; }
    }
}