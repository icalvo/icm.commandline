namespace Icm.CommandLine
{
    public class RequiredSpec : UnnamedParameter
    {
        public RequiredSpec(string name) : base(name)
        {
        }

        public RequiredSpec(string name, string description) : base(name, description)
        {
        }
    }
}