using System;
using System.Collections.Generic;
using System.Linq;

namespace Icm.CommandLine
{
    internal class ValuesStore
	{
		private readonly Dictionary<string, List<string>> store = new Dictionary<string, List<string>>();

		private readonly List<string> mainStore = new List<string>();

		public bool ContainsShortName(string parameterShortName)
		{
			return store.ContainsKey(parameterShortName);
		}

		public void ForceParameter(string shortName, string longName)
		{
			if (shortName == null) {
				throw new ArgumentNullException("shortName");
			}

            if (longName == null) {
				throw new ArgumentNullException("longName");
			}

            if (shortName.Length >= longName.Length) {
				throw new ArgumentException(string.Format(Resources.CommandLine.S_ERR_SHORT_LONGER_THAN_LONG, shortName, longName));
			}

		    if (!this.ContainsShortName(shortName))
		    {
                store.Add(shortName, new List<string>());
                store.Add(longName, new List<string>());
            }

		}

		public void AddValue(string shortName, string longName, string val)
		{
			store[shortName].Add(val);
			store[longName].Add(val);
		}

		public void AddMainValue(string val)
		{
			mainStore.Add(val);
		}

	    public string Value(string key)
	    {
            if (this.ContainsShortName(key))
            {
                if (Values(key).Count == 0)
                {
                    return null;
                }

                return Values(key).ElementAt(0);
            }

            // Case 1: No -e
            return null;
	    }

		public ICollection<string> Values(string s) {
			if (store.ContainsKey(s)) {
				return store[s].AsReadOnly();
			}

			return (new List<string>()).AsReadOnly();
		}

		public ICollection<string> MainValues {
		    get
		    {
		        return mainStore.AsReadOnly();
		    }
		}
	}
}
