using System;

namespace Icm.CommandLine
{
	public class UndefinedParameterException : Exception
	{
        public UndefinedParameterException(string parameterName)
            : base("Undefined parameter " + parameterName)
		{
            this.Option = parameterName;
		}

	    public string Option { get; private set; }
	}

}
