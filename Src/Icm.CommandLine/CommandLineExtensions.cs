using System;
using System.Collections.Generic;
using res = Icm.CommandLine.Resources.CommandLine;

namespace Icm.CommandLine
{
    public static class CommandLineExtensions
    {
        /// <summary>
        ///  Prints the set of instructions for a CommandLine.
        /// </summary>
        /// <remarks></remarks>
        public static void Instructions(this CommandLineParser cmdline, string executableName = null)
        {
            Console.Write("{0}: ", res.S_HELP_USAGE);

            executableName = executableName ?? System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            Console.Write(executableName);

            foreach (NamedParameter paramLoopVariable in cmdline.NamedParameters)
            {
                var param = paramLoopVariable;
                if (param.IsRequired)
                {
                    Console.Write(" --{0}", param.LongName);
                }
                else
                {
                    Console.Write(" [--{0}", param.LongName);
                }

                Console.Write(UnnamedParametersLine(param.SubParameters));

                if (!param.IsRequired)
                {
                    Console.Write("]");
                }
            }
            var mainParameters = cmdline.MainParameters;
            Console.Write(UnnamedParametersLine(mainParameters));

            Console.WriteLine();
            Console.WriteLine();
            Console.Write("{0}:", res.S_NAMED_PARAMETERS);
            Console.WriteLine();

            foreach (var namedParameter in cmdline.NamedParameters)
            {
                Console.WriteLine("  -{0}{1}", namedParameter.ShortName, UnnamedParametersLine(namedParameter.SubParameters));
                Console.WriteLine("  --{0}{1}", namedParameter.LongName, UnnamedParametersLine(namedParameter.SubParameters));

                Console.WriteLine("      {0}{1}", namedParameter.IsRequired ? Resources.CommandLine.S_HELP_REQUIRED : "", namedParameter.Description);
            }

            Console.WriteLine();

            foreach (var keyLoopVariable in mainParameters.ParametersDictionary.Keys)
            {
                var key = keyLoopVariable;
                Console.WriteLine("  {0}", key);
                Console.WriteLine("      {0}", mainParameters.ParametersDictionary[key].Description);
            }

            Console.WriteLine();
            Console.WriteLine(res.S_SUBARGUMENTS_SEPARATED);
        }

        private static string UnnamedParametersLine(UnnamedParametersConfig unnamedParametersConfig)
        {
            var subargs = new List<string>();

            unnamedParametersConfig.Iterate(
                param => subargs.Add(param.Name),
                param => subargs.Add(string.Format("[{0}]", param.Name)),
                param => subargs.Add(string.Format("{{{0}}}", param.Name))
                );

            if (subargs.Count == 0)
            {
                return "";
            }

            return " " + string.Join(" ", subargs);
        }

        /// <summary>
        ///  Prints the set of values for a CommandLine.
        /// </summary>
        /// <remarks></remarks>
        public static void Debug(this CommandLineParser cmdline)
        {
            foreach (var paramLoopVariable in cmdline.NamedParameters)
            {
                var param = paramLoopVariable;
                if (cmdline.IsPresent(param.ShortName))
                {
                    Console.Write("{0}=", param.LongName);
                    Console.WriteLine("({0})", cmdline.GetValues(param.ShortName).JoinStr("", "\"", ",", "\"", ""));
                }
                else
                {
                    Console.Write("{0}=", param.LongName);
                    Console.WriteLine("UNDEFINED");
                }
            }
            Console.WriteLine(cmdline.MainValues.JoinStr("(", "\"", ",", "\"", ")"));

            Console.WriteLine();
        }

        public static int? GetInteger(this CommandLineParser cmdline, string key)
        {
            if (cmdline.IsPresent(key))
            {
                return Convert.ToInt32(cmdline.GetValue(key));
            }

            return null;
        }
    }
}