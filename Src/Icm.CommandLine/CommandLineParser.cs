using System;
using System.Collections.Generic;
using System.Linq;
using res = Icm.CommandLine.Resources.CommandLine;

namespace Icm.CommandLine
{
    /// <summary>
    ///     This class abstracts a command line following a GNU-like syntax.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         This class performs a full processing
    ///         of command line arguments. It assumes the following general syntax:
    ///     </para>
    ///     <para>program -namedparam1 subarg1a subarg1b -namedparam2 subarg2a subarg2b [-] unnamedparam1 unnamedparam2</para>
    ///     <para>
    ///         To use it, create an instance, configure named and unnamed parameters, and process a command line
    ///         with <see cref="CommandLineParser.Parse"></see>, which must be passed a String array like
    ///         the one provided by <see cref="Environment.GetCommandLineArgs"></see> (In fact, calling
    ///         <see cref="CommandLineParser.Parse"></see>
    ///         without arguments will process the result of <see cref="Environment.GetCommandLineArgs"></see>). After the
    ///         processing,
    ///         <see cref="ParsingErrors"></see> will provide the errors found. If no error is found,
    ///         <see cref="CommandLineParser.IsPresent"></see> will say if a named parameter is present,
    ///         <see cref="CommandLineParser.GetValue"></see> and <see cref="CommandLineParser.GetValues"></see> will
    ///         return subarguments of some named parameter, and
    ///         <see cref="CommandLineParser.MainValue"></see> and <see cref="CommandLineParser.MainValues"></see> will
    ///         return the unnamed parameters.
    ///     </para>
    ///     <para>NOTE: By default, a <see cref="CommandLineParser"></see> will feature a --help parameter.</para>
    ///     <para>
    ///         <see cref="CommandLineExtensions.Instructions"></see> paints a colorized help text, based on the configured
    ///         parameters.
    ///     </para>
    ///     <para>
    ///         <see cref="CommandLineParser"></see> only includes validation of optional/required arguments
    ///         or subarguments, existence of parameters, and proper syntax (including double hyphen for
    ///         long parameter name, single hyphen for single parameter name, and optional single hyphen alone
    ///         for separating the named parameter part from the unnamed arguments.
    ///     </para>
    ///     <para>
    ///         For any more advanced or specific validations, and of course for actually doing something with
    ///         the arguments, the client will have to do it herself.
    ///     </para>
    ///     <para>
    ///         Also, <see cref="CommandLineParser.Parse"></see> do NOT fail, so the client will have to use
    ///         <see cref="CommandLineParser.HasErrors"></see> and <see cref="ParsingErrors"></see> properties and react
    ///         accordingly.
    ///     </para>
    ///     <para>For an example, see Icm.CommandLine.Sample package.</para>
    /// </remarks>
    public class CommandLineParser
    {
        #region Attributes

        /// <summary>
        ///     Named parameters hash table, for finding a parameter given a name. For example, "h" and
        ///     "help" are keys and they point to the same named parameter.
        /// </summary>
        private readonly Dictionary<string, NamedParameter> namedParameterDictionary =
            new Dictionary<string, NamedParameter>();

        private readonly List<NamedParameter> namedParameters = new List<NamedParameter>();

        // Values for each of the named and unnamed parameters
        private readonly ValuesStore valuesStore = new ValuesStore();

        private TokenListEnumerator tokensEnumerator;

        // Configuration for unnamed (main) parameters

        // Some named parameters can change the configuration for unnamed parameters,
        // rendering them optional. This variable will hold either the original, user provided configuration
        // if none of those named parameters is found, or MainArgumentsConfig.None.

        #endregion Attributes

        public CommandLineParser()
        {
            this.Main(UnnamedParametersConfig.Optional());
            this.ParsingErrors = new List<string>();
            this.AddHelpParameter();
        }

        #region Query

        #region " Before parse "

        public NamedParameter this[string parameterName]
        {
            get
            {
                if (namedParameterDictionary.ContainsKey(parameterName))
                {
                    return namedParameterDictionary[parameterName];
                }

                throw new UndefinedParameterException(parameterName);
            }
        }

        public bool IsDeclaredHelp
        {
            get { return IsPresent(res.S_DEFAULT_HELP_LONG); }
        }

        #endregion " Before process "

        #region " After parse "

        public IList<string> ParsingErrors { get; private set; }

        public ICollection<string> MainValues
        {
            get { return valuesStore.MainValues; }
        }

        public string MainValue
        {
            get
            {
                if (valuesStore.MainValues.Count == 0)
                {
                    return null;
                }

                return valuesStore.MainValues.ElementAt(0);
            }
        }

        public bool IsPresent(string parameterName)
        {
            if (namedParameterDictionary.ContainsKey(parameterName))
            {
                return valuesStore.ContainsShortName(parameterName);
            }

            throw new UndefinedParameterException(parameterName);
        }

        public ICollection<string> GetValues(string parameterName)
        {
            if (namedParameterDictionary.ContainsKey(parameterName))
            {
                return valuesStore.Values(parameterName);
            }

            throw new UndefinedParameterException(parameterName);
        }

        public string GetValue(string namedParameter)
        {
            if (namedParameterDictionary.ContainsKey(namedParameter))
            {
                return valuesStore.Value(namedParameter);
            }

            throw new UndefinedParameterException(namedParameter);
        }

        public bool HasErrors()
        {
            return ParsingErrors.Count > 0;
        }

        #endregion " After parse "

        #endregion Query

        #region Fluid configuration

        #region Named parameters

        public List<NamedParameter> NamedParameters
        {
            get { return namedParameters; }
        }

        /// <summary>
        ///     Adds an optional named parameter.
        /// </summary>
        /// <param name="sShortName"></param>
        /// <param name="sLongName"></param>
        /// <param name="sDescription"></param>
        /// <param name="subargs"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser Optional(string sShortName, string sLongName, string sDescription,
            params UnnamedParameter[] subargs)
        {
            var o = new NamedParameter(sShortName, sLongName, sDescription, false, ParamsBuilder.Build(subargs));

            NamedParameter(o);

            return this;
        }

        /// <summary>
        ///     Adds a required named parameter.
        /// </summary>
        /// <param name="sShortName"></param>
        /// <param name="sLongName"></param>
        /// <param name="sDescription"></param>
        /// <param name="subargs"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser Required(string sShortName, string sLongName, string sDescription,
            params UnnamedParameter[] subargs)
        {
            var o = new NamedParameter(sShortName, sLongName, sDescription, true, ParamsBuilder.Build(subargs));

            NamedParameter(o);

            return this;
        }


        /// <summary>
        ///     Adds an optional named parameter.
        /// </summary>
        /// <param name="sShortName"></param>
        /// <param name="sLongName"></param>
        /// <param name="sDescription"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser Optional(string sShortName, string sLongName, string sDescription)
        {
            return Optional(sShortName, sLongName, sDescription, UnnamedParametersConfig.None());
        }

        /// <summary>
        ///     Adds an optional named parameter.
        /// </summary>
        /// <param name="sShortName"></param>
        /// <param name="sLongName"></param>
        /// <param name="sDescription"></param>
        /// <param name="subargs"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser Optional(string sShortName, string sLongName, string sDescription,
            UnnamedParametersConfig subargs)
        {
            var o = new NamedParameter(sShortName, sLongName, sDescription, false, subargs);

            NamedParameter(o);

            return this;
        }

        /// <summary>
        ///     Adds a required named parameter.
        /// </summary>
        /// <param name="sShortName"></param>
        /// <param name="sLongName"></param>
        /// <param name="sDescription"></param>
        /// <param name="subargs"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser Required(string sShortName, string sLongName, string sDescription,
            UnnamedParametersConfig subargs)
        {
            var o = new NamedParameter(sShortName, sLongName, sDescription, true, subargs);

            NamedParameter(o);

            return this;
        }

        /// <summary>
        ///     Adds a named parameter.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser NamedParameter(NamedParameter o)
        {
            namedParameterDictionary.Add(o.ShortName.ToLower(), o);
            namedParameterDictionary.Add(o.LongName.ToLower(), o);
            namedParameters.Add(o);

            return this;
        }

        #endregion " Named parameters "

        #region Main parameters

        public UnnamedParametersConfig MainParameters { get; private set; }

        /// <summary>
        ///     Establishes that unnamed parameters are optional. This is the default value.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser Main(params UnnamedParameter[] config)
        {
            this.MainParameters = ParamsBuilder.Build(config);
            return this;
        }

        /// <summary>
        ///     Establishes that unnamed parameters are optional. This is the default value.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public CommandLineParser Main(UnnamedParametersConfig config)
        {
            this.MainParameters = config;
            return this;
        }

        #endregion " Main parameters "

        #endregion Fluid configuration

        #region Parsing

        /// <summary>
        ///     Process arguments from the application settings and an array of strings
        /// </summary>
        /// <param name="commandLineArgs">The arguments.</param>
        /// <remarks>
        ///     <para>If args is nothing, <see cref="Environment.GetCommandLineArgs"></see> result will used.</para>
        ///     <para>Application settings can be also null and in that case they will be ignored.</para>
        ///     <para>The args array takes precedence over the application settings.</para>
        /// </remarks>
        public void Parse(params string[] commandLineArgs)
        {
            List<string> commandLineArgsExceptExecutable = commandLineArgs.Skip(1).ToList();

            this.tokensEnumerator = new TokenListEnumerator(commandLineArgsExceptExecutable);

            ProcessTokens();

            this.CheckMainValuesCount();

            CheckRequiredValues();
        }

        private void ProcessTokens()
        {
            while (!this.tokensEnumerator.Finished() && !this.HasErrors())
            {
                this.ProcessToken();
            }
        }

        private void ProcessToken()
        {
            if (this.tokensEnumerator.TokenIsParameter)
            {
                this.ProcessNamed(this.tokensEnumerator.CurrentWithoutDashes);
            }
            else if (this.tokensEnumerator.IsSeparator)
            {
                this.tokensEnumerator.MoveNext();
                this.ProcessMain();
            }
            else 
            {
                this.ProcessMain();
            }
        }

        private void CheckMainValuesCount()
        {
            if (valuesStore.MainValues.Count > this.MainParameters.Maximum)
            {
                ParsingErrors.Add(res.S_ERR_NOARGS_WHEN_REQUIRED);
            }

            if (valuesStore.MainValues.Count < this.MainParameters.Minimum)
            {
                ParsingErrors.Add(res.S_ERR_NOARGS_WHEN_REQUIRED);
            }
        }

        private void CheckRequiredValues()
        {
            foreach (NamedParameter parameter in namedParameters)
            {
                if (parameter.IsRequired && !valuesStore.ContainsShortName(parameter.ShortName))
                {
                    ParsingErrors.Add(string.Format(res.S_ERR_ARG_REQUIRED, parameter.LongName, parameter.ShortName));
                }
            }
        }

        public void Clean()
        {
            this.ParsingErrors.Clear();
        }

        protected void AddHelpParameter()
        {
            var helpParameter = new NamedParameter(res.S_DEFAULT_HELP_SHORT, res.S_DEFAULT_HELP_LONG,
                res.S_DEFAULT_HELP_DESC, false, UnnamedParametersConfig.None())
            {
                MakesMainArgumentsIrrelevant = true
            };
            this.NamedParameter(helpParameter);
        }

        private void ProcessNamed(string parameterName)
        {
            this.tokensEnumerator.MoveNext();
            NamedParameter parameter;

            if (!this.namedParameterDictionary.TryGetValue(parameterName.ToLower(), out parameter))
            {
                this.ParsingErrors.Add(string.Format("{0}: {1}", res.S_ERR_OPTION_NOT_EXIST, parameterName));
                return;
            }

            if (parameter.MakesMainArgumentsIrrelevant)
            {
                this.Main(UnnamedParametersConfig.Optional());
            }

            this.ProcessSubParameters(parameter);
        }

        private void ProcessMain()
        {
            while (!this.tokensEnumerator.Finished())
            {
                this.valuesStore.AddMainValue(this.tokensEnumerator.Token);
                this.tokensEnumerator.MoveNext();
            }
        }

        private List<string> RetrieveParameterValues(NamedParameter parameter)
        {
            var values = new List<string>();

            while (
                !this.tokensEnumerator.Finished() && 
                this.tokensEnumerator.Token[0] != '-' && 
                parameter.SubParameters.Exceeds(values.Count + 1))
            {
                values.Add(this.tokensEnumerator.Token);
                this.tokensEnumerator.MoveNext();
            }

            return values;
        }

        private void ProcessSubParameters(NamedParameter parameter)
        {
            List<string> values = RetrieveParameterValues(parameter);

            int boundArgsCount = parameter.SubParameters.Maximum.HasValue
                ? parameter.SubParameters.Maximum.Value
                : parameter.SubParameters.Minimum;

            SubArgument[] subArgumentList =
                CreateInitializedArray<SubArgument>(length: Math.Max(boundArgsCount, values.Count));

            FillSubParameters(parameter, subArgumentList);

            FillValues(values, subArgumentList);

            this.StoreValues(parameter, subArgumentList);

            if (values.Count < parameter.SubParameters.Minimum)
            {
                ParsingErrors.Add(string.Format(res.S_ERR_NOT_ENOUGH_SUBARGS, parameter.LongName));
            }
        }

        private T[] CreateInitializedArray<T>(int length) where T : new()
        {
            var result = new T[length];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new T();
            }

            return result;
        }

        private void StoreValues(NamedParameter parameter, IEnumerable<SubArgument> subArgumentList)
        {
            this.valuesStore.ForceParameter(parameter.ShortName, parameter.LongName);
            foreach (SubArgument subArg in subArgumentList)
            {
                if (subArg.IsParameterWithValue())
                {
                    valuesStore.AddValue(parameter.ShortName, parameter.LongName, subArg.Value);
                }
            }
        }

        private static void FillValues(IEnumerable<string> values, SubArgument[] subArgumentList)
        {
            int i = 0;
            foreach (string value in values)
            {
                subArgumentList[i].Value = value;
                i++;
            }
        }

        private static void FillSubParameters(NamedParameter parameter, SubArgument[] subArgumentList)
        {
            int i = 0;
            parameter.SubParameters.Iterate(
                param => 
                {
                    subArgumentList[i].SubParameter = param;
                    i++;
                },
                param =>
                {
                    subArgumentList[i].SubParameter = param;
                    i++;
                },
                param =>
                {
                    while (i < subArgumentList.Count())
                    {
                        subArgumentList[i].SubParameter = param;
                        i++;
                    }
                });
        }

        private class SubArgument
        {
            public UnnamedParameter SubParameter { private get; set; }

            public string Value { get; set; }

            //public bool IsRequiredWithoutValue()
            //{
            //    return
            //        SubParameter != null &&
            //        SubParameter.Type == SubParameterType.Required &&
            //        Value == null;
            //}

            public bool IsParameterWithValue()
            {
                return SubParameter != null && Value != null;
            }
        }

        #endregion Parsing
    }
}