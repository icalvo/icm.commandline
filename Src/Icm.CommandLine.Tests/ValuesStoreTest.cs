using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Icm.CommandLine;
using NUnit.Framework;

///<summary>
///This is a test class for ValuesStore
///</summary>
[TestFixture, Category("Icm")]
[ExcludeFromCodeCoverage]
public class ValuesStoreTest
{

	[TestCase(null, "asdf", ExpectedException = typeof(ArgumentNullException))]
	[TestCase("asdf", null, ExpectedException = typeof(ArgumentNullException))]
	[TestCase("a", "a", ExpectedException = typeof(ArgumentException))]
	[TestCase("a", "asdf")]
	public void AddTest(string shortName, string longName)
	{
		ValuesStore store = new ValuesStore();

		store.ForceParameter(shortName, longName);

		Assert.That(store.ContainsShortName(shortName));
		Assert.That(store.ContainsShortName(longName));
	}

	[TestCase(null, "asdf", ExpectedException = typeof(ArgumentNullException))]
	[TestCase("asdf", null, ExpectedException = typeof(ArgumentNullException))]
	[TestCase("a", "a", ExpectedException = typeof(ArgumentException))]
	[TestCase("asdf", "a", ExpectedException = typeof(ArgumentException))]
	[TestCase("asdf", "qwer", ExpectedException = typeof(ArgumentException))]
	[TestCase("a", "asdf")]
	public void AddValue(string shortName, string longName)
	{
		ValuesStore store = new ValuesStore();

		store.ForceParameter(shortName, longName);

		Assert.That(store.ContainsShortName(shortName));
		Assert.That(store.ContainsShortName(longName));
	}

}
