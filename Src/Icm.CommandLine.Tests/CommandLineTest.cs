using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

using NUnit.Framework;

namespace Icm.CommandLine.Tests
{
    [TestFixture]
    [Category("Icm")]
    [ExcludeFromCodeCoverage]
    public class CommandLineTest
    {
        #region Public Methods and Operators

        [Test]
        public void ArrayOperator_WithUndefinedOption_Fails()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.Throws<UndefinedParameterException>(() => { NamedParameter res = cln["f"]; });

            Assert.Throws<UndefinedParameterException>(() => { NamedParameter res = cln["foo"]; });
        }

        [Test]
        [Category("Icm")]
        public void Debug_PrintsCorrectOutput()
        {
            CommandLineParser cln = GetCommandLineExampleForDebug();

            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            cln.Parse("example.exe", "-b", "4", "-x", "--", "32");
            cln.Debug();
            Assert.That(stringWriter.ToString(), Is.EqualTo(@"help=UNDEFINED
base=(""4"")
exponent=UNDEFINED
extensions=()
parameter4=UNDEFINED
(""32"")

"));
        }

        [Test]
        [Category("Icm")]
        public void GetValue_WithUndefinedOption_Fails()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.Throws<UndefinedParameterException>(() => { string res = cln.GetValue("f"); });

            Assert.Throws<UndefinedParameterException>(() => { string res = cln.GetValue("foo"); });
        }

        [Test]
        [Category("Icm")]
        public void Parse_WithSeparator_SeparatesMainArgumentsCorrectly()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "4", "-x", "--", "32");

            AssertNoErrors(cln);
            Assert.That(cln.IsPresent("x"));
            Assert.AreEqual(0, cln.GetValues("x").Count);
            Assert.That(cln.MainValue == "32");
        }

        [Test]
        [Category("Icm")]
        public void GetValuesTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "4", "-x", "*.vb", "*.asdf", "--", "32");

            AssertNoErrors(cln);
            Assert.That(cln.IsPresent("x"));
            Assert.AreEqual(2, cln.GetValues("x").Count);
            Assert.That(cln.GetValues("x").Contains("*.vb"));
            Assert.That(cln.GetValues("x").Contains("*.asdf"));
            Assert.That(cln.MainValue == "32");
        }

        [Test]
        [Category("Icm")]
        public void GetValues_WithManySubargs()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-e", "2", "32");

            ICollection<string> res = cln.GetValues("e");

            Assert.IsTrue(res.Contains("2"));
            Assert.IsTrue(res.Contains("32"));
        }

        [Test]
        [Category("Icm")]
        public void GetValues_WithUndefinedOptionTest_Fails()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.Throws<UndefinedParameterException>(() => { ICollection<string> res = cln.GetValues("f"); });

            Assert.Throws<UndefinedParameterException>(() => { ICollection<string> res = cln.GetValues("foo"); });
        }

        [Test]
        [Category("Icm")]
        public void InexistentOptionTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-s", "2", "32");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void InstructionsTest()
        {
            CommandLineParser cln = GetCommandLineExampleForInstructions();

            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            cln.Instructions("mytext.exe");
            Assert.That(stringWriter.ToString(),
                Is.EqualTo(@"Usage: mytext.exe [--help] --base base [--exponent exponent [subarg1]] [--extensions {extensions}] [--parameter4 {parameter4subargs}] param1 param2 [param3] [param4]

Options:
  -h
  --help
      This will print help
  -b base
  --base base
      REQUIRED. Base of the logarithm
  -e exponent [subarg1]
  --exponent exponent [subarg1]
      Exponent of the logarithm
  -x {extensions}
  --extensions {extensions}
      Extensions
  -p {parameter4subargs}
  --parameter4 {parameter4subargs}
      Parameter 4

  param1
      Param description 1
  param2
      Param description 2
  param3
      Param description 3
  param4
      Param description 4

Subarguments must be separated by spaces.
"));
        }

        [Test]
        [Category("Icm")]
        public void IsPresent_FalseWithLongTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.That(!cln.IsPresent("e"));
            Assert.AreEqual(cln.GetValue("e"), null);
            Assert.That(!cln.IsPresent("exponent"));
            Assert.AreEqual(cln.GetValue("exponent"), null);
        }

        [Test]
        [Category("Icm")]
        public void IsPresent_TrueWithLongTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "--base", "2", "32");

            Assert.That(cln.IsPresent("b"));
            Assert.That(cln.IsPresent("base"));
        }

        [Test]
        [Category("Icm")]
        public void IsPresent_TrueWithShortTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.That(cln.IsPresent("b"));
            Assert.That(cln.IsPresent("base"));
        }

        [Test]
        [Category("Icm")]
        public void IsPresent_WithOptionNoSubargs_Success()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-q", "2", "32");

            bool res = cln.IsPresent("q");
        }

        [Test]
        [Category("Icm")]
        public void IsPresent_WithUndefinedOption_Fails()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.Throws<UndefinedParameterException>(() => { bool res = cln.IsPresent("f"); });

            Assert.Throws<UndefinedParameterException>(() => { bool res = cln.IsPresent("foo"); });
        }

        [Test]
        [Category("Icm")]
        public void MainDefault_Success1()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.That(cln.MainValue, Is.EqualTo("32"));
            Assert.That(cln.MainValues.Count, Is.EqualTo(1));
        }

        [Test]
        [Category("Icm")]
        public void MainDefault_Success2()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.AtMost(arg, arg, arg));

            cln.Parse("example.exe", "-b", "2", "32", "asdf");

            AssertNoErrors(cln);
            Assert.That(cln.MainValue, Is.EqualTo("32"));
            Assert.That(cln.MainValues.Count, Is.EqualTo(2));
            Assert.That(cln.MainValues.ElementAt(0), Is.EqualTo("32"));
            Assert.That(cln.MainValues.ElementAt(1), Is.EqualTo("asdf"));
        }

        [Test]
        [Category("Icm")]
        public void MainAtLeastTest_Fail()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.AtLeast(arg, arg, arg, arg));

            cln.Parse("example.exe", "-b", "2", "32", "asdf");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void MainAtLeastTest_Success()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.AtLeast(arg, arg, arg, arg));

            cln.Parse("example.exe", "-b", "2", "32", "asdf", "qwer");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainAtMost2Test()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.AtMost(arg, arg));

            cln.Parse("example.exe", "-b", "2", "32", "asdf", "qwer");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void MainAtMostTest_Success()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.AtMost(arg, arg));

            cln.Parse("example.exe", "-b", "2", "32", "asdf");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainBetween1Test()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Between(2, arg, arg, arg));

            cln.Parse("example.exe", "-b", "2", "32");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void MainBetween2Test()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Between(2, arg, arg, arg));

            cln.Parse("example.exe", "-b", "2", "32", "asdf");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainBetween3Test()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Between(2, arg, arg, arg));

            cln.Parse("example.exe", "-b", "2", "main1", "main2", "main3", "main4");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void MainNoneTest_Fail()
        {
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.None());

            cln.Parse("example.exe", "-b", "2", "main1");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void MainNoneTest_Success()
        {
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.None());

            cln.Parse("example.exe", "-b", "2");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainOptional1Test()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Optional(arg));

            cln.Parse("example.exe", "-b", "2");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainOptional2Test()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Optional(arg));

            cln.Parse("example.exe", "-b", "2", "main1", "main2");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainRequired1Test_Fail()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Required(arg));

            cln.Parse("example.exe", "-b", "2");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void MainRequired1Test_Success1()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Required(arg));

            cln.Parse("example.exe", "-b", "2", "main1");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainRequired1Test_Success2()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Required(arg));

            cln.Parse("example.exe", "-b", "2", "main1", "main2");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainRequired2Test_Fail()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Required(arg, arg));

            cln.Parse("example.exe", "-b", "2");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void MainRequired2Test_Success1()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Required(arg, arg));

            cln.Parse("example.exe", "-b", "2", "main1");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void MainRequired2Test_Success2()
        {
            var arg = new UnnamedParameter("arg", "");
            CommandLineParser cln = GetCommandLineExample2().Main(UnnamedParametersConfig.Required(arg, arg));

            cln.Parse("example.exe", "-b", "2", "main1", "main2");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void ProcessArguments_NotEnoughValuesTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "-e", "--", "32");

            Assert.That(cln.HasErrors());
        }

        [Test]
        [Category("Icm")]
        public void ProcessArguments_SeparatesMainArgumentsTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "32");

            AssertNoErrors(cln);
        }

        [Test]
        [Category("Icm")]
        public void ProcessArguments_TooManyValuesTest()
        {
            CommandLineParser cln = GetCommandLineExample();

            cln.Parse("example.exe", "-b", "2", "-e", "2", "3", "4", "--", "32");

            Assert.That(cln.HasErrors());
        }

        #endregion

        #region Methods

        private static void AssertNoErrors(CommandLineParser cln)
        {
            Assert.That(!cln.HasErrors(), "Unexpected parsing errors: {0}", string.Join(", ", cln.ParsingErrors));
        }

        private static CommandLineParser GetCommandLineExample()
        {
            var commandLine = new CommandLineParser();

            commandLine
                .Required("b", "base", "Base of the logarithm", new RequiredSpec("base"))
                .Optional(
                    "e",
                    "exponent",
                    "Exponent of the logarithm",
                    new RequiredSpec("exponent"), new OptionalSpec("subarg1"))
                .Optional("x", "extensions", "Extensions", UnnamedParametersConfig.Optional("extensions"))
                .Optional(
                    "p",
                    "parameter4",
                    "Parameter 4",
                    new OptionalSpec("parameter4subargs", "Description or parameter 4 subarguments"))
                .Optional("q", "parameter5", "Parameter 5 without subargs")
                .Main(new RequiredSpec("number", "Number which logarithm is extracted"));

            return commandLine;
        }

        private static CommandLineParser GetCommandLineExample2()
        {
            var commandLine = new CommandLineParser();

            commandLine.Required("b", "base", "Base of the logarithm", UnnamedParametersConfig.Exactly("base"))
                .Optional("e", "exponent", "Exponent of the logarithm", UnnamedParametersConfig.Exactly("exponent"))
                .Main(UnnamedParametersConfig.None());

            return commandLine;
        }

        private static CommandLineParser GetCommandLineExampleForDebug()
        {
            var commandLine = new CommandLineParser();

            commandLine.Required("b", "base", "Base of the logarithm", UnnamedParametersConfig.Exactly("base"))
                .Optional(
                    "e",
                    "exponent",
                    "Exponent of the logarithm",
                    UnnamedParametersConfig.Between(1, new UnnamedParameter("exponent"), new UnnamedParameter("subarg1")))
                .Optional("x", "extensions", "Extensions", UnnamedParametersConfig.Optional("extensions"))
                .Optional(
                    "p",
                    "parameter4",
                    "Parameter 4",
                    UnnamedParametersConfig.Optional("parameter4subargs", "Description or parameter 4 subarguments"))
                .Main(UnnamedParametersConfig.Exactly(new UnnamedParameter("number", "Number which logarithm is extracted")));

            return commandLine;
        }

        private static CommandLineParser GetCommandLineExampleForInstructions()
        {
            var commandLine = new CommandLineParser();

            commandLine.Required("b", "base", "Base of the logarithm", UnnamedParametersConfig.Exactly("base"))
                .Optional(
                    "e",
                    "exponent",
                    "Exponent of the logarithm",
                    UnnamedParametersConfig.Between(1, new UnnamedParameter("exponent"), new UnnamedParameter("subarg1")))
                .Optional("x", "extensions", "Extensions", UnnamedParametersConfig.Optional("extensions"))
                .Optional(
                    "p",
                    "parameter4",
                    "Parameter 4",
                    UnnamedParametersConfig.Optional("parameter4subargs", "Description or parameter 4 subarguments"))
                .Main(UnnamedParametersConfig.Between(2,
                    new UnnamedParameter("param1", "Param description 1"),
                    new UnnamedParameter("param2", "Param description 2"),
                    new UnnamedParameter("param3", "Param description 3"),
                    new UnnamedParameter("param4", "Param description 4")));

            return commandLine;
        }

        #endregion
    }
}