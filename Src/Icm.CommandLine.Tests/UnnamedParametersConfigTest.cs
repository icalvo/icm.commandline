using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace Icm.CommandLine.Tests
{
    [TestFixture]
    [Category("Icm")]
    [ExcludeFromCodeCoverage]
    public class UnnamedParametersConfigTest
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WithNullArguments()
        {
            var config = new UnnamedParametersConfig(0, 1, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Ctor_WithNegativeMinimum()
        {
            var config = new UnnamedParametersConfig(-4, 1, new UnnamedParameter[0]);
        }

        [Test]
        [ExpectedException(typeof(Exception))]
        public void Ctor_WithMinimumGreaterThanMaximum()
        {
            var config = new UnnamedParametersConfig(3, 1, new UnnamedParameter[0]);
        }
    }
}